package knshiro;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
/**
 * 测试shiro认证
 * @author Administrator
 *
 */
import org.junit.Test;
public class ShiroTest {
	@Test
	public void testLoginByRealm() throws Exception{
		//1:创建SecurityManager 工厂对象:加载配置文件，创建工厂对象
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-cryptography.ini");
		//2:通过工厂对象，创建SecurityManager对象
		SecurityManager securityManager = factory.getInstance();
		//3:将securityManager 绑定到当前运行环境中；让系统随时随地访问securityManager对象
		SecurityUtils.setSecurityManager(securityManager);
		
		//4:创建当前登录的主体，注意：此时主体没有经过认证
		Subject subject = SecurityUtils.getSubject();
		
		//5:收集主体登录的身份、凭证，即账号密码
		//参数1：将要登录的用户名，参数2：登录用户的密码
		UsernamePasswordToken token = new UsernamePasswordToken("zhangsan","666");
		
		//6:主体登录
		try {
			subject.login(token);
		} catch (Exception e) {
			//登录失败
			e.printStackTrace();
		}
		
		
		//7:判断登录是否成功
		System.out.println("验证登录是否成功："+subject.isAuthenticated());
		//8:登出（注销）
		subject.logout();
		System.out.println("验证登录是否成功："+subject.isAuthenticated());
	}
}
